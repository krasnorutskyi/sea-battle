namespace Sea_Battle
{
    public class SupportShip : Ship, ISupport
    {
        public SupportShip(int size, int speed, bool isHorizontal)
            : base(size, speed, isHorizontal)
        {
            this.type = "Support";
        }

        public void Fix(int xCoordinate, int yCoordinate)
        {
            throw new System.NotImplementedException();
        }
    }
}