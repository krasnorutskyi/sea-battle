namespace Sea_Battle
{
    public class MixedShip : Ship, IMilitary, ISupport
    {
        public MixedShip(int size, int speed, bool isHorizontal)
            : base(size, speed, isHorizontal)
        {
            this.type = "Mixed";
        }

        public void Fix(int xCoordinate, int yCoordinate)
        {
            throw new System.NotImplementedException();
        }

        public void Shoot(int xCoordinate, int yCoordinate)
        {
            throw new System.NotImplementedException();
        }
    }
}