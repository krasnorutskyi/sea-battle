﻿using System;

namespace Sea_Battle
{
    class Program
    {
        static void Main(string[] args)
        {
            var a = new MilitaryShip(2, 10, true);
            var map = new GameMap();
            map.Locate(a, 2, 1);
            a.Status();
            var b = new SupportShip(3, 10, false);
            map.Locate(b, 1, -3);
            map[1, 3, 4].Status();
            map.Status();
        }
    }
}