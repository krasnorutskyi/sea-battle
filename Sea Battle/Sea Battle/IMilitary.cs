namespace Sea_Battle
{
    public interface IMilitary
    {
        public void Shoot(int xCoordinate, int yCoordinate);
    }
}