namespace Sea_Battle
{
    public interface ISupport
    {
        public void Fix(int xCoordinate, int yCoordinate);
    }
}