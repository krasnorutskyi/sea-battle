using System;
using System.Collections.Generic;

namespace Sea_Battle
{
    public class GameMap
    {
        private Ship[,] map;
        private List<Ship> ships = new List<Ship>();

        public GameMap(int size = 10)
        {
            if (size % 2 == 0)
            {
                this.Size = size;
            }
            else
            {
                this.Size = size + 1;
            }

            this.map = new Ship[size, size];
        }

        private int Size { get; }

        public Ship this[int absX, int absY, int quadrant]
        {
            get
            {
                try
                {
                    this.Convert(ref absX, ref absY, quadrant);
                    if (this.map[absY, absX] is null)
                    {
                        throw new Exception("There is no ship in chose cell");
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Error: {e.Message}");
                }

                return this.map[absY, absX];
            }
        }

        public void Status()
        {
            Console.WriteLine("*************** Game Field ***************");
            ShowGameField();
            Console.WriteLine("******************************************");
            Console.WriteLine("Game field contains {0} ships. Size of field: {1}", this.ships.Count.ToString(), this.Size.ToString());
            foreach (var ship in this.ships)
            {
                ship.Status();
            }
        }

        private void ShowGameField()
        {
            for (int i = 0; i < Size; i++)
            {
                for (int j = 0; j < Size; j++)
                {
                    if (map[i, j] is not null)
                    {
                        Console.Write(" 1 ");
                    }
                    else
                    {
                        Console.Write(" 0 ");
                    }
                }
                Console.WriteLine();
            }
            
        }

        public void Locate(Ship ship, int xCoordinate, int yCoordinate)
        {
            var initialXCoordinate = xCoordinate;
            var initialYCoordinate = yCoordinate;
            this.Convert(ref xCoordinate, ref yCoordinate);

            try
            {
                if (initialXCoordinate + ship.Size > this.Size - 1 || initialXCoordinate == 0 || initialYCoordinate + ship.Size > this.Size - 1 || initialYCoordinate == 0)
                {
                    throw new Exception("Invalid coordinates or cell is taken.");
                }
                else
                {
                    if (!this.IsNotTaken(xCoordinate, yCoordinate, ship))
                    {
                        Console.WriteLine("You can't put your ship here");
                    }
                    else
                    {
                        if (ship.IsHorizontal)
                        {
                            for (int i = xCoordinate; i < xCoordinate + ship.Size; i++)
                            {
                                this.map[yCoordinate, i] = ship;
                            }

                            ship.EndXCoordinate = initialXCoordinate + ship.Size - 1;
                            ship.EndYCoordinate = initialYCoordinate;
                        }
                        else
                        {
                            for (int i = yCoordinate; i < yCoordinate + ship.Size; i++)
                            {
                                this.map[i, xCoordinate] = ship;
                            }

                            ship.EndXCoordinate = initialXCoordinate;
                            ship.EndYCoordinate = initialYCoordinate + ship.Size - 1;
                        }

                        this.DefineQuadrant(xCoordinate, yCoordinate, ship);
                        ship.StartXCoordinate = initialXCoordinate;
                        ship.StartYCoordinate = initialYCoordinate;
                        ship.Distance = this.Distance(ship);
                        this.AddShipToList(ship);

                        this.ships.Sort((x, y) => x.Distance.CompareTo(y.Distance));
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"Error: {e.Message}");
            }
        }

        private void DefineQuadrant(int xCoordinate, int yCoordinate, Ship ship)
        {
            if (xCoordinate < 0 && yCoordinate < 0)
            {
                ship.Quadrant = 3;
            }
            else if (xCoordinate > 0 && yCoordinate > 0)
            {
                ship.Quadrant = 1;
            }
            else if (xCoordinate < 0 && yCoordinate > 0)
            {
                ship.Quadrant = 2;
            }
            else
            {
                ship.Quadrant = 4;
            }
        }

        private void Convert(ref int absXCoordinate, ref int absYCoordinate, int quadrant)
        {
            if (quadrant == 1)
            {
                absXCoordinate += 4;
                absYCoordinate = Math.Abs(absYCoordinate - 5);
            }
            else if (quadrant == 2)
            {
                absXCoordinate = 5 - absXCoordinate;
                absYCoordinate = Math.Abs(absYCoordinate - 5);
            }
            else if (quadrant == 3)
            {
                absXCoordinate = 5 - absXCoordinate;
                absYCoordinate += 4;
            }
            else
            {
                absXCoordinate += 4;
                absYCoordinate += 4;
            }
        }

        private void Convert(ref int xCoordinate, ref int yCoordinate)
                {
                    if (xCoordinate > 0)
                    {
                        xCoordinate += 4;
                    }
                    else
                    {
                        xCoordinate += 5;
                    }

                    if (yCoordinate > 0)
                    {
                        yCoordinate = Math.Abs(yCoordinate - 5);
                    }
                    else
                    {
                        yCoordinate = Math.Abs(yCoordinate) + 4;
                    }
                }

        private double Distance(Ship ship)
        {
            var closestXCoordinate = Math.Min(Math.Abs(ship.StartXCoordinate), Math.Abs(ship.EndXCoordinate));
            var closestYCoordinate = Math.Min(Math.Abs(ship.StartYCoordinate), Math.Abs(ship.EndYCoordinate));
            var distance = Math.Sqrt(Math.Pow(closestXCoordinate, 2) + Math.Pow(closestYCoordinate, 2));
            return distance;
        }

        private bool IsNotTaken(int xCoordinate, int yCoordinate, Ship ship)
        {
            if (ship.IsHorizontal)
            {
                for (int i = xCoordinate; i < xCoordinate + ship.Size; i++)
                {
                    if (this.map[yCoordinate, i] is not null)
                    {
                        return false;
                    }
                }
            }
            else
            {
                for (int i = yCoordinate; i < yCoordinate + ship.Size; i++)
                {
                    if (this.map[i, xCoordinate] is not null)
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        private void AddShipToList(Ship ship)
        {
            this.ships.Add(ship);
        }
    }
}