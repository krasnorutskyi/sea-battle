using System;

namespace Sea_Battle
{
    public abstract class Ship
    {
        private protected string type;

        public Ship(int size, int speed, bool isHorizontal)
        {
            this.Size = size;
            this.Speed = speed;
            this.IsHorizontal = isHorizontal;
            this.Range = size * 3;
        }

        public int Size { get; set; }

        public int Speed { get; set; }

        public int Range { get; set; }

        public bool IsHorizontal { get; set; }

        public string Type
        {
            get { return this.type; }
        }

        public int StartXCoordinate { get; set; }

        public int StartYCoordinate { get; set; }

        public int EndXCoordinate { get; set; }

        public int EndYCoordinate { get; set; }

        public int Quadrant { get; set; }

        public double Distance { get; set; }

        public void Status()
        {
           Console.WriteLine(
               "{0} ship with a speed of {1} and size of {2}. Range has value of {3}. Coordinates of start and end: ({4}; {5}) and ({6}; {7})",
               this.Type,
               this.Speed.ToString(),
               this.Size.ToString(),
               this.Range.ToString(),
               this.StartXCoordinate.ToString(),
               this.StartYCoordinate.ToString(),
               this.EndXCoordinate.ToString(),
               this.EndYCoordinate.ToString());
        }

        public void Move()
        {
            Console.WriteLine("Done");
        }

        public static bool operator ==(Ship s1, Ship s2)
        {
            return s1.Size == s2.Size && s1.Speed == s2.Speed && s1.Type == s2.Type;
        }
        
        public static bool operator !=(Ship s1, Ship s2)
        {
            return !(s1 == s2);
        }
    }
}