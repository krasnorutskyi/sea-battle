namespace Sea_Battle
{
    public class MilitaryShip : Ship, IMilitary
    {
        public MilitaryShip(int size, int speed, bool isHorizontal)
            : base(size, speed, isHorizontal)
        {
            this.type = "Military";
        }

        public void Shoot(int xCoordinate, int yCoordinate)
        {
            throw new System.NotImplementedException();
        }
    }
}